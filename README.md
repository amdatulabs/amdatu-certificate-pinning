# amdatu-certificate-pinning

Provides an API and utilities for pinning certificates.

Certificate pins can be used to verify the authenticity of certificates without
having to rely on public certificate authorities (CAs), for example, because
you are not able to establish such a trust relationship.

## Usage

The basic usage for certificate pins is:

* for any certificate you trust, you create a `Pin`. This pin represents a
  cryptographic hash of either the certificate's public key or the entire
  certificate. You should store these pins for later use;
* when you want to verify a certificate, you can use the pins created earlier
  to verify that the certificate corresponds to these pins. If a match is found
  you can trust the certificate. A `PinningX509TrustManager` is provided that
  can be used to verify certificates against any number of pins. 

For convenience, a couple of utility methods are provided to easily create or
use pinned certificates, see the `util` package for that.

## License

This project is licensed under Apache License v2.

