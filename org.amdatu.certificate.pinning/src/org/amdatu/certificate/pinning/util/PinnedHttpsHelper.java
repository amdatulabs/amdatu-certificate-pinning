/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.certificate.pinning.util;

import static org.amdatu.certificate.pinning.util.PinnedSSLContextFactory.createContext;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.amdatu.certificate.pinning.Pin;

/**
 * Provides a couple of utility methods to work with plain {@link URL}s in combination with {@link Pin}s.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class PinnedHttpsHelper {

    /**
     * Creates a new {@link PinnedHttpsHelper} instance.
     */
    private PinnedHttpsHelper() {
        // Not used.
    }

    /**
     * Opens a connection to a given HTTPS {@link URL} using the given {@link Pin}s.
     * 
     * @param url the URL to open, cannot be <code>null</code> and should have a protocol of "https";
     * @param pins the array with {@link Pin}s to use, cannot be <code>null</code> or empty.
     * @return a {@link HttpsURLConnection}, never <code>null</code>. The connection is not yet opened, you should call
     *         {@link URLConnection#connect()} yourself!
     * @throws IOException in case opening the connection failed;
     * @throws GeneralSecurityException in case the creation or initialization of the {@link SSLContext} failed.
     */
    public static HttpsURLConnection openConnection(URL url, Pin... pins) throws IOException, GeneralSecurityException {
        return openConnection(url, Arrays.asList(pins));
    }

    /**
     * Opens a connection to a given HTTPS {@link URL} using the given {@link Pin}s.
     * 
     * @param url the URL to open, cannot be <code>null</code> and should have a protocol of "https";
     * @param pins the collection with {@link Pin}s to use, cannot be <code>null</code> or empty.
     * @return a {@link HttpsURLConnection}, never <code>null</code>. The connection is not yet opened, you should call
     *         {@link URLConnection#connect()} yourself!
     * @throws IOException in case opening the connection failed;
     * @throws GeneralSecurityException in case the creation or initialization of the {@link SSLContext} failed.
     */
    public static HttpsURLConnection openConnection(URL url, Collection<Pin> pins) throws IOException, GeneralSecurityException {
        if (url == null) {
            throw new IllegalArgumentException("URL cannot be null!");
        }
        if (pins == null || pins.isEmpty()) {
            throw new IllegalArgumentException("Pins cannot be null or empty!");
        }
        if (!"https".equals(url.getProtocol())) {
            throw new IllegalArgumentException("Only https-URLs can be pinned!");
        }

        SSLContext sslContext = createContext(pins);

        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
        urlConnection.setAllowUserInteraction(false);
        urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());

        return urlConnection;
    }
}
