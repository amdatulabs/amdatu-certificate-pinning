/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning.util;

import static org.amdatu.certificate.pinning.util.PinnedSSLContextFactory.createContext;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import org.amdatu.certificate.pinning.Pin;

/**
 * Provides a {@link SSLSocketFactory} implementation that can work with pinned certificates.
 * <p>
 * Use this factory if you want to be able to authenticate servers based on pinned certificates.
 * </p>
 */
public class PinnedSSLSocketFactory extends SSLSocketFactory {
    private final SSLSocketFactory m_socketFactory;

    /**
     * Creates a new {@link PinnedSSLSocketFactory} instance.
     * 
     * @param pins the collection of (server) pins to trust, cannot be <code>null</code> or empty;
     * @param keyManagers the optional key managers to use for providing the client certificate(s).
     */
    public PinnedSSLSocketFactory(Collection<Pin> pins, KeyManager... keyManagers) throws GeneralSecurityException {
        if (pins == null || pins.isEmpty()) {
            throw new IllegalArgumentException("Pins cannot be null or empty!");
        }

        SSLContext sslContext = createContext(pins, keyManagers);
        m_socketFactory = sslContext.getSocketFactory();
    }

    /**
     * Creates a new {@link PinnedSSLSocketFactory} instance for .
     * 
     * @param pins the server pins to trust, cannot be <code>null</code> and at least one {@link Pin} should be
     *        provided.
     */
    public PinnedSSLSocketFactory(Pin... pins) throws GeneralSecurityException {
        this(pins == null ? null : Arrays.asList(pins));
    }

    @Override
    public Socket createSocket() throws IOException {
        return m_socketFactory.createSocket();
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return m_socketFactory.createSocket(host, port);
    }

    @Override
    public Socket createSocket(InetAddress host, int port, InetAddress localHost, int localPort) throws IOException {
        return m_socketFactory.createSocket(host, port, localHost, localPort);
    }

    @Override
    public Socket createSocket(Socket s, InputStream consumed, boolean autoClose) throws IOException {
        return m_socketFactory.createSocket(s, consumed, autoClose);
    }

    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        return m_socketFactory.createSocket(s, host, port, autoClose);
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        return m_socketFactory.createSocket(host, port);
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
        return m_socketFactory.createSocket(host, port, localHost, localPort);
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return m_socketFactory.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return m_socketFactory.getSupportedCipherSuites();
    }
}
