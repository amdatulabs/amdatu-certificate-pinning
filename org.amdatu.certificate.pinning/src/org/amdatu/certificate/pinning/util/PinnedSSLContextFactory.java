/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning.util;

import java.security.GeneralSecurityException;
import java.util.Collection;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.amdatu.certificate.pinning.Pin;
import org.amdatu.certificate.pinning.PinningX509TrustManager;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class PinnedSSLContextFactory {

    /**
     * Creates a new {@link PinnedSSLContextFactory} instance.
     */
    private PinnedSSLContextFactory() {
        // Not used.
    }

    public static SSLContext createContext(Collection<Pin> pins, KeyManager... keyManagers) throws GeneralSecurityException {
        TrustManager tm = new PinningX509TrustManager(pins);

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(keyManagers, new TrustManager[] { tm }, null);

        return sslContext;
    }
}
