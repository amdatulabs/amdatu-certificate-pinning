/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning.util;

import static org.amdatu.certificate.pinning.util.PinnedSSLContextFactory.createContext;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;

import org.amdatu.certificate.pinning.Pin;

/**
 * Provides a {@link SSLServerSocketFactory} implementation that can work with pinned certificates.
 * <p>
 * Use this factory if you want to be able to authenticate clients based on pinned certificates.
 * </p>
 */
public class PinnedSSLServerSocketFactory extends SSLServerSocketFactory {
    private final SSLServerSocketFactory m_socketFactory;

    /**
     * Creates a new {@link PinnedSSLServerSocketFactory} instance.
     * 
     * @param the {@link KeyManager} to use for obtaining X.509 certificates used in SSL transactions, cannot be
     *        <code>null</code> if the SSL transaction is to complete successfully;
     * @param pins the collection of (client) pins to accept, cannot be <code>null</code> or empty.
     */
    public PinnedSSLServerSocketFactory(KeyManager keyManager, Collection<Pin> pins) throws GeneralSecurityException {
        this(new KeyManager[] { keyManager }, pins);
    }

    /**
     * Creates a new {@link PinnedSSLServerSocketFactory} instance.
     * 
     * @param the {@link KeyManager} to use for obtaining X.509 certificates used in SSL transactions, cannot be
     *        <code>null</code> if the SSL transaction is to complete successfully;
     * @param pins the client pins to accept, cannot be <code>null</code> and at least one {@link Pin} should be
     *        provided if a SSL transaction is to be completed successfully.
     */
    public PinnedSSLServerSocketFactory(KeyManager keyManager, Pin... pins) throws GeneralSecurityException {
        this(new KeyManager[] { keyManager }, pins == null ? null : Arrays.asList(pins));
    }

    /**
     * Creates a new {@link PinnedSSLServerSocketFactory} instance.
     * 
     * @param the {@link KeyManager}s to use for obtaining X.509 certificates used in SSL transactions, cannot be
     *        <code>null</code> and at least one {@link KeyManager} should be provided for the SSL transaction to
     *        complete successfully;
     * @param pins the collection of (client) pins to accept, cannot be <code>null</code> or empty.
     */
    public PinnedSSLServerSocketFactory(KeyManager[] keyManagers, Collection<Pin> pins) throws GeneralSecurityException {
        if ((keyManagers == null) || keyManagers.length < 1) {
            throw new IllegalArgumentException("At least one key manager is needed!");
        }
        if (pins == null || pins.isEmpty()) {
            throw new IllegalArgumentException("Pins cannot be null or empty!");
        }

        SSLContext sslContext = createContext(pins, keyManagers);
        m_socketFactory = sslContext.getServerSocketFactory();
    }

    /**
     * Creates a new {@link PinnedSSLServerSocketFactory} instance.
     * 
     * @param the {@link KeyManager}s to use for obtaining X.509 certificates used in SSL transactions, cannot be
     *        <code>null</code> and at least one {@link KeyManager} should be provided for the SSL transaction to
     *        complete successfully;
     * @param pins the client pins to accept, cannot be <code>null</code> and at least one {@link Pin} should be
     *        provided if a SSL transaction is to be completed successfully.
     */
    public PinnedSSLServerSocketFactory(KeyManager[] keyManagers, Pin... pins) throws GeneralSecurityException {
        this(keyManagers, pins == null ? null : Arrays.asList(pins));
    }

    @Override
    public ServerSocket createServerSocket() throws IOException {
        return m_socketFactory.createServerSocket();
    }

    @Override
    public ServerSocket createServerSocket(int port) throws IOException {
        return m_socketFactory.createServerSocket(port);
    }

    @Override
    public ServerSocket createServerSocket(int port, int backlog) throws IOException {
        return m_socketFactory.createServerSocket(port, backlog);
    }

    @Override
    public ServerSocket createServerSocket(int port, int backlog, InetAddress ifAddress) throws IOException {
        return m_socketFactory.createServerSocket(port, backlog, ifAddress);
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return m_socketFactory.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return m_socketFactory.getSupportedCipherSuites();
    }
}
