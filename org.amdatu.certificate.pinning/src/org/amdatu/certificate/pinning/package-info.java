/**
 * Defines the API for certificate pinning.
 * <p>
 * Certificate pins can be used to verify the authenticity of certificates without having
 * to rely on public certificate authorities (CAs), for example, because you are not able
 * to establish a trust relationship.
 * </p>
 * <p>
 * The basic usage of certificate pinning is as follows:
 * </p>
 * <ul>
 * <li>for any certificate you trust, a {@link org.amdatu.certificate.pinning.Pin} is 
 * created, which represents a hashcode (or fingerprint) of that certificate. These
 * pins are stored in such way that you can retrieve them any time later;</li>
 * <li>when you want to verify a certificate is trusted, you can use one or more of
 * the earlier created {@link org.amdatu.certificate.pinning.Pin}s to see if any of
 * them matches.</li>
 * </ul> 
 */
package org.amdatu.certificate.pinning;

