/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.net.ssl.X509TrustManager;

/**
 * Provides a {@link X509TrustManager} implementation that is able to deal with pinned certificates.
 * <p>
 * Note that this implementation only takes the end-entity into account.
 * </p>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class PinningX509TrustManager implements X509TrustManager {
    private static final X509Certificate[] NO_ACCEPTED_ISSUERS = new X509Certificate[0];
    
    private final List<Pin> m_pins;

    /**
     * Creates a new {@link PinningX509TrustManager} instance.
     */
    public PinningX509TrustManager(Collection<Pin> pins) {
        if (pins.isEmpty()) {
            throw new IllegalArgumentException("Need at least one pin!");
        }
        m_pins = new ArrayList<>(pins);
    }

    /**
     * Creates a new {@link PinningX509TrustManager} instance.
     */
    public PinningX509TrustManager(Pin... pins) {
        this(Arrays.asList(pins));
    }

    @Override
    public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
        checkCertificateTrust(certs[0]);
    }

    @Override
    public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
        checkCertificateTrust(certs[0]);
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return NO_ACCEPTED_ISSUERS;
    }

    private void checkCertificateTrust(X509Certificate cert) throws CertificateException {
        if (!isPinned(cert)) {
            throw new CertificateException(String.format("Certificate (DN=%s issued by %s) not pinned!", cert.getSubjectX500Principal(), cert.getIssuerX500Principal()));
        }
    }

    private boolean isPinned(X509Certificate cert) throws CertificateException {
        boolean result = false;
        for (Pin pin : m_pins) {
            result |= pin.appliesTo(cert);
        }
        return result;
    }
}
