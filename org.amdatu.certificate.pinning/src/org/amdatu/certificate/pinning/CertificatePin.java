/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning;

import java.security.cert.Certificate;
import java.security.cert.CertificateException;

/**
 * Represents a {@link Pin} for an entire {@link Certificate}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class CertificatePin extends Pin {

    /**
     * Creates a new {@link CertificatePin} instance for the given hash-algorithm and pin.
     * 
     * @param algName the name of the hashing algorithm to use, such as "sha-256", cannot be <code>null</code> or empty;
     * @param pin the <b>expected</b> pin value of the certificate, cannot be <code>null</code>.
     */
    public CertificatePin(String algName, byte[] pin) {
        super(algName, pin);
    }

    /**
     * Creates a new {@link CertificatePin} instance for calculating a {@link Pin} for a given certificate.
     * 
     * @param algName the name of the hashing algorithm to use, such as "sha-256", cannot be <code>null</code> or empty;
     * @param cert the certificate to calculate the {@link Pin} for, cannot be <code>null</code>.
     */
    public CertificatePin(String algName, Certificate cert) throws CertificateException {
        super(algName, cert);
    }

    @Override
    protected byte[] getPinSubject(Certificate certificate) throws CertificateException {
        return certificate.getEncoded();
    }
}
