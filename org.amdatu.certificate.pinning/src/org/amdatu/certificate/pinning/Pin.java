/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Defines a pinned certificate.
 * <p>
 * Pinning a certificate means that a secure hash-function is applied to only the public key of a
 * certificate, or the entire certificate. The resulting hashcode can be used to verify whether or
 * not the certificate is as expected. This way, you can validate a certificate without having to
 * rely on public CAs, for example, if you can not or do not want to establish a trust relation to
 * any CA.<br/>
 * The exact details of what hash-function is to be used is implementation specific, but
 * implementations should use cryptographic secure hash-functions to ensure that hashcodes can not
 * easily be falsied. In the same way, the hashcode itself is also considered an implementation
 * detail and as such not further specified.
 * </p>
 * <p>
 * More information on the background of certificate pinning can be found on the
 * <a href="https://www.owasp.org/index.php/Certificate_and_Public_Key_Pinning">OWASP site</a>.
 * </p>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public abstract class Pin {
    /** Message digest algorithms we definitely do *not* want to allow as they are not proven to be secure enough. */
    protected static final Set<String> REJECTED_ALGORITHMS = new HashSet<>(Arrays.asList("md2", "md5", "sha1", "sha-1"));

    private final MessageDigest m_digester;
    private final byte[] m_pin;

    /**
     * Creates a new {@link Pin} instance for the given (pre-calculated) pin and hashing algorithm.
     * 
     * @param algName the name of the hashing algorithm to use, such as "sha-256", cannot be <code>null</code> or empty;
     * @param pin the <b>expected</b> pin value of the certificate, cannot be <code>null</code>.
     */
    protected Pin(String algName, byte[] pin) {
        if (algName == null || "".equals(algName.trim())) {
            throw new IllegalArgumentException("Invalid algorithm name: cannot be null or empty!");
        }
        if (pin == null || pin.length < 1) {
            throw new IllegalArgumentException("Invalid pin: cannot be null or empty!");
        }
        m_digester = create(algName);
        m_pin = Arrays.copyOf(pin, pin.length);
    }

    /**
     * Creates a new {@link Pin} instance for the given hashing algorithm and certificate.
     * <p>
     * Use this constructor to compute a {@link Pin} for a certificate!
     * </p>
     * 
     * @param algName the name of the hashing algorithm to use, such as "sha-256", cannot be <code>null</code> or empty;
     * @param cert the certificate to calculate the {@link Pin} for, cannot be <code>null</code>.
     */
    protected Pin(String algName, Certificate cert) throws CertificateException {
        if (algName == null || "".equals(algName.trim())) {
            throw new IllegalArgumentException("Invalid algorithm name: cannot be null or empty!");
        }
        if (cert == null) {
            throw new IllegalArgumentException("Invalid certificate: cannot be null!");
        }
        m_digester = create(algName);
        m_pin = calculatePin(cert);
    }

    /**
     * Factory method for creating a proper hash function of a certain algorithm.
     * 
     * @param algName the name of the hashing algorithm, such as "sha-256", cannot be <code>null</code> or empty.
     * @return a new {@link MessageDigest} instance for the given algorithm name, never <code>null</code>.
     * @throws IllegalArgumentException in case the given algorithm name is empty, or not considered secure enough.
     */
    protected static MessageDigest create(String algName) {
        if (algName == null || "".equals(algName.trim())) {
            throw new IllegalArgumentException("Algorithm cannot be null or empty!");
        }

        String algorithm = algName.trim().toLowerCase(Locale.US);
        if (REJECTED_ALGORITHMS.contains(algorithm)) {
            throw new IllegalArgumentException("Algorithm not secure enough: " + algorithm);
        }

        try {
            return MessageDigest.getInstance(algorithm);
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Invalid message digest name: " + algorithm);
        }
    }

    /**
     * Validates whether or not this pin applies to a given certificate.
     * <p>
     * This method will return <code>true</code> if, and only if, the hashcode represented by
     * this pin is exactly equal to the hashcode of (the public key provided by) the given
     * certificate.</p>
     * 
     * @param certificate the certificate to check, cannot be <code>null</code>.
     * @return <code>true</code> if this pin represents the same hashcode as the given certificate,
     *         <code>false</code> otherwise.
     * @throws IllegalArgumentException in case the given certificate was <code>null</code>;
     * @throws CertificateException in case the given certificate has an unknown encoding or is for
     *         some other reason invalid regarding its contents.
     */
    public final boolean appliesTo(Certificate certificate) throws CertificateException {
        if (certificate == null) {
            throw new IllegalArgumentException("Certificate cannot be null!");
        }
        byte[] pin = calculatePin(certificate);
        return MessageDigest.isEqual(m_pin, pin);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Arrays.equals(m_pin, ((Pin) obj).m_pin);
    }

    /**
     * Returns the actual (hashed) byte values of this {@link Pin}.
     * 
     * @return the hashed pin value, never <code>null</code>.
     */
    public final byte[] getPin() {
        return Arrays.copyOf(m_pin, m_pin.length);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(m_pin);
    }

    /**
     * @param certificate the certificate to calculate the pin for, cannot be <code>null</code>.
     * @return the calculated pin, never <code>null</code>.
     * @throws CertificateException in case the given certificate has an unknown encoding or is for
     *         some other reason invalid regarding its contents.
     */
    protected final byte[] calculatePin(Certificate certificate) throws CertificateException {
        byte[] pin;
        // MessageDigest is stateful, meaning that we need to ensure that these pins are safe to
        // use from a multi-threading perspective...
        synchronized (m_digester) {
            pin = m_digester.digest(getPinSubject(certificate));
            m_digester.reset();
        }
        return pin;
    }

    /**
     * Returns the subject for which the pin is determined of a given certificate, which essentially is a
     * property/attribute of the given attribute.
     * <p>
     * Implementation do <b>not</b> need to calculate the hash of the pin subject, this is done by the caller of this
     * method.</p>
     * 
     * @param certificate the certificate to return the pin subject, cannot be <code>null</code>.
     * @return the pin subject, as byte array, never <code>null</code>.
     * @throws CertificateException in case obtaining the pin subject failed somehow.
     */
    protected abstract byte[] getPinSubject(Certificate certificate) throws CertificateException;
}
