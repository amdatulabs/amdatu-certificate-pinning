/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning;

import static org.amdatu.certificate.pinning.PinTestUtils.generateMockCertificate;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test cases for {@link PinningX509TrustManager}
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class PinningX509TrustManagerTest {
    private static final String ALGORITHM = "sha-256";
    private static final String AUTH_TYPE = "RSA";

    @Rule
    public ExpectedException m_expected = ExpectedException.none();

    @Test(expected = IllegalArgumentException.class)
    public void testCreationWithoutPinsFail() throws Exception {
        new PinningX509TrustManager();
    }

    @Test
    public void testCheckPinnedCertificateOk() throws Exception {
        X509Certificate cert1 = generateMockCertificate();
        X509Certificate cert2 = generateMockCertificate();

        Pin pin = new CertificatePin(ALGORITHM, cert1);

        PinningX509TrustManager trustMgr = new PinningX509TrustManager(pin);

        // Should be trusted: pinned...
        trustMgr.checkClientTrusted(new X509Certificate[] { cert1 }, AUTH_TYPE);

        m_expected.expect(CertificateException.class);
        // Should *not* be trusted: not pinned...
        trustMgr.checkClientTrusted(new X509Certificate[] { cert2 }, AUTH_TYPE);
    }

    @Test
    public void testCheckPinnedEndEntityCertificateOk() throws Exception {
        X509Certificate cert1 = generateMockCertificate();
        X509Certificate cert2 = generateMockCertificate();

        Pin pin = new PublicKeyPin(ALGORITHM, cert1);

        PinningX509TrustManager trustMgr = new PinningX509TrustManager(pin);

        m_expected.expect(CertificateException.class);
        // Should *not* be trusted: only the first element (= end entity) is taken into account...
        trustMgr.checkClientTrusted(new X509Certificate[] { cert2, cert1 }, AUTH_TYPE);
    }
}
