/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning;

import static org.amdatu.certificate.pinning.PinTestUtils.generateMockCertificate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import org.junit.Test;

/**
 * Test cases for {@link PublicKeyPin}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class PublicKeyPinTest {
    private static final String ALGORITHM = "sha-256";

    @Test(expected = IllegalArgumentException.class)
    public void testGenerateNullAlgorithmFail() throws Exception {
        new PublicKeyPin(null, generateMockCertificate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGenerateNullPinFail() throws Exception {
        new PublicKeyPin(ALGORITHM, (byte[]) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGenerateNullCertificateFail() throws Exception {
        new PublicKeyPin(ALGORITHM, (Certificate) null);
    }

    @Test
    public void testGeneratePinOk() throws Exception {
        X509Certificate cert1 = generateMockCertificate();
        X509Certificate cert2 = generateMockCertificate();

        Pin pin1 = new PublicKeyPin(ALGORITHM, cert1);
        Pin pin2 = new PublicKeyPin(ALGORITHM, cert2);

        assertTrue(pin1.appliesTo(cert1));
        assertFalse(pin1.appliesTo(cert2));

        assertTrue(pin2.appliesTo(cert2));
        assertFalse(pin2.appliesTo(cert1));
    }
}
