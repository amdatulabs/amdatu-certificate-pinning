/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Random;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.X509KeyManager;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import sun.security.x509.AlgorithmId;
import sun.security.x509.CertificateAlgorithmId;
import sun.security.x509.CertificateSerialNumber;
import sun.security.x509.CertificateValidity;
import sun.security.x509.CertificateVersion;
import sun.security.x509.CertificateX509Key;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@SuppressWarnings("restriction")
public final class PinTestUtils {
    private static final String DEFAULT_CHAIN = "default";

    /**
     * Creates a custom Hamcrest {@link Matcher} that matches exceptions, or any of its <em>suppressed</em> exceptions.
     * 
     * @param type the exception type to look for, cannot be <code>null</code>;
     * @param msg the message that should be contained in the exception message.
     * @return a Hamcrest {@link Matcher} that matches against the given exception type and message.
     */
    public static Matcher<Exception> matchExceptionWithMessage(final Class<? extends Exception> type, final String msg) {
        return new BaseMatcher<Exception>() {
            @Override
            public boolean matches(Object arg) {
                if (type.isInstance(arg)) {
                    String actualMsg = type.cast(arg).getMessage();
                    if (actualMsg != null && actualMsg.contains(msg)) {
                        return true;
                    }
                }
                if (Exception.class.isInstance(arg)) {
                    Throwable[] suppressedExs = ((Exception) arg).getSuppressed();
                    for (Throwable suppressed : suppressedExs) {
                        if (matches(suppressed)) {
                            return true;
                        }
                    }
                }
                return false;
            }

            @Override
            public void describeTo(Description arg) {
                // Nop
            }
        };
    }

    /**
     * USE THIS METHOD FOR TESTING PURPOSES ONLY!
     * 
     * @return a self-signed certificate, never <code>null</code>.
     */
    public static X509Certificate createSelfSignedCertificate(String dn) throws Exception {
        X509KeyManager km = createX509KeyManager(dn);
        return getDefaultCertificate(km);
    }

    /**
     * USE THIS METHOD FOR TESTING PURPOSES ONLY!
     * 
     * @return a {@link X509KeyManager} implementation that can verify the given certificate.
     */
    public static X509KeyManager createX509KeyManager(String dn) throws GeneralSecurityException {
        KeyPair pair = KeyPairGenerator.getInstance("RSA").generateKeyPair();

        // For some reason, we should always provide a non-null, non-empty password, even if we're not using it...
        char[] pwd = "unused".toCharArray();

        try {
            X509Certificate cert = createSelfSignedCertificate(pair, dn);

            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, pwd);
            ks.setEntry(DEFAULT_CHAIN, new PrivateKeyEntry(pair.getPrivate(), new X509Certificate[] { cert }), new PasswordProtection(pwd));

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, pwd);

            return (X509KeyManager) kmf.getKeyManagers()[0];
        }
        catch (IOException e) {
            throw new RuntimeException("I/O exception should not occur!", e);
        }
    }

    /**
     * @return a minimal mocked implementation of {@link X509Certificate}, that contains random data for its public key
     *         and encoded version. Use this method if the certificate content is not relevant.
     */
    public static X509Certificate generateMockCertificate() throws Exception {
        Random rnd = new Random();

        byte[] certData = new byte[128];
        rnd.nextBytes(certData);

        byte[] pkData = new byte[128];
        rnd.nextBytes(pkData);

        PublicKey pk = mock(PublicKey.class);
        when(pk.getEncoded()).thenReturn(pkData);

        X509Certificate mock = mock(X509Certificate.class);
        when(mock.getPublicKey()).thenReturn(pk);
        when(mock.getEncoded()).thenReturn(certData);
        return mock;
    }

    /**
     * @return a fake {@link Pin} based on random data useful for testing broken trustrelations.
     */
    public static Pin generateMockPin() throws Exception {
        X509Certificate fakeCert = generateMockCertificate();
        return new CertificatePin("SHA-256", fakeCert);
    }

    /**
     * Extracts the end-entity from the 'default' chain in the given key manager.
     * 
     * @param km the key manager to get the default chain from, cannot be <code>null</code>.
     * @return the end-entity certificate, never <code>null</code>.
     */
    public static X509Certificate getDefaultCertificate(X509KeyManager km) {
        X509Certificate[] chain = km.getCertificateChain(DEFAULT_CHAIN);
        assertNotNull("No certificate chain found for alias 'default'!", chain);
        assertTrue("Empty certificate chain found for alias 'default'!", chain.length > 0);
        return chain[0];
    }

    /**
     * USE THIS METHOD FOR TESTING PURPOSES ONLY!
     * 
     * @return a self-signed certificate, never <code>null</code>.
     */
    private static X509Certificate createSelfSignedCertificate(KeyPair pair, String dn) throws GeneralSecurityException, IOException {
        String signAlgorithm = "SHA1withRSA";

        long now = System.currentTimeMillis();
        Date from = new Date(now - 1000L);
        Date to = new Date(now + 4000L);

        X500Name owner = new X500Name(dn);

        X509CertInfo info = new X509CertInfo();
        info.set(X509CertInfo.SUBJECT, owner);
        info.set(X509CertInfo.ISSUER, owner);
        info.set(X509CertInfo.VALIDITY, new CertificateValidity(from, to));
        info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(100001));
        info.set(X509CertInfo.KEY, new CertificateX509Key(pair.getPublic()));
        info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));

        AlgorithmId algo = new AlgorithmId(AlgorithmId.sha1WithRSAEncryption_oid);
        info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algo));

        // Sign the cert to identify the algorithm that's used.
        X509CertImpl cert = new X509CertImpl(info);
        cert.sign(pair.getPrivate(), signAlgorithm);

        // Update the algorithm, and resign.
        algo = (AlgorithmId) cert.get(X509CertImpl.SIG_ALG);
        info.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, algo);

        cert = new X509CertImpl(info);
        cert.sign(pair.getPrivate(), signAlgorithm);

        return cert;
    }
}
