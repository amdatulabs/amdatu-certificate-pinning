/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning.util;

import static org.amdatu.certificate.pinning.PinTestUtils.createX509KeyManager;
import static org.amdatu.certificate.pinning.PinTestUtils.generateMockPin;
import static org.amdatu.certificate.pinning.PinTestUtils.getDefaultCertificate;
import static org.amdatu.certificate.pinning.PinTestUtils.matchExceptionWithMessage;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.X509KeyManager;

import org.amdatu.certificate.pinning.CertificatePin;
import org.amdatu.certificate.pinning.Pin;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test cases for {@link PinnedSSLServerSocketFactory} which tests whether we can authenticate clients based on the
 * {@link Pin}s of their certificate.
 */
public class PinnedSSLServerSocketFactoryTest extends AbstractPinnedSSLSocketFactoryTest {
    @Rule
    public ExpectedException m_expected = ExpectedException.none();

    private X509KeyManager m_serverKM;
    private X509KeyManager m_clientKM;
    private Pin m_serverPin;
    private Pin m_clientPin;

    @Before
    public void setUp() throws Exception {
        m_serverKM = createX509KeyManager("cn=server");
        m_serverPin = new CertificatePin(PIN_ALGORITHM, getDefaultCertificate(m_serverKM));

        m_clientKM = createX509KeyManager("cn=client");
        m_clientPin = new CertificatePin(PIN_ALGORITHM, getDefaultCertificate(m_clientKM));
    }

    /**
     * Tests that we can successfully create a two-way SSL connection based on pinned certificates.
     * <p>
     * Note that <b>both</b> the client and server certificate are (correctly) pinned!
     * </p>
     */
    @Test
    public void testCreatePinnedClientSocketConnectionOk() throws Exception {
        Pin trustedServerPin = m_clientPin;
        Pin trustedClientPin = m_serverPin;

        // Let the server trust the client PIN and the other way around...
        performPingPongTest(m_serverKM, trustedServerPin, m_clientKM, trustedClientPin);
    }

    /**
     * Tests that we cannot create a two-way SSL connection if the client certificate is not pinned by the server.
     * <p>
     * Note that the client <b>does</b> have pinned the server certificate, but the server has a invalid pin for the
     * client!
     * </p>
     */
    @Test
    public void testCreateUnpinnedClientSocketConnectionFail() throws Exception {
        Pin trustedServerPin = generateMockPin();
        Pin trustedClientPin = m_serverPin;

        // because the server pin does not match the pin of the client's certificate, we should not be able to
        // successfully ping-pong...
        m_expected.expect(matchExceptionWithMessage(SSLHandshakeException.class, "Certificate (DN=CN=client issued by CN=client) not pinned!"));

        performPingPongTest(m_serverKM, trustedServerPin, m_clientKM, trustedClientPin);
    }
}
