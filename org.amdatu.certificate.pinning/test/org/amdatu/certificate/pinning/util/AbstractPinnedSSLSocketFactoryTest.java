/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.certificate.pinning.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509KeyManager;

import org.amdatu.certificate.pinning.Pin;

/**
 * Base class for testing the pinned versions of {@link SSLSocketFactory} and {@link SSLServerSocketFactory}.
 */
abstract class AbstractPinnedSSLSocketFactoryTest {
    static class ClientSide implements Callable<String> {
        private final SSLSocket m_socket;
        private final CountDownLatch m_start;
        private final CountDownLatch m_stop;

        public ClientSide(SSLSocket socket, CountDownLatch start, CountDownLatch stop) {
            m_socket = socket;
            m_start = start;
            m_stop = stop;
        }

        @Override
        public String call() throws Exception {
            m_start.await();

            m_socket.connect(new InetSocketAddress(SERVER_PORT));

            try (OutputStream os = m_socket.getOutputStream(); InputStream is = m_socket.getInputStream()) {
                System.out.println("[CLIENT] Connected to server. Writing command...");

                os.write(PDU_COMMAND);
                os.flush();

                System.out.println("[CLIENT] Command written, awaiting response...");

                byte[] buf = new byte[PDU_RESPONSE.length];
                int read = is.read(buf);
                if (read != buf.length) {
                    throw new RuntimeException("[CLIENT] Not enough bytes read: " + read + ", expected " + buf.length + " bytes, got " + read + "!");
                }

                String response = new String(buf);

                System.out.println("[CLIENT] Response obtained! Ending client...");

                return response;
            }
            catch (Exception e) {
                System.err.println("[CLIENT] Caught exception with message: " + e.getMessage());
                throw e;
            }
            finally {
                m_stop.countDown();
            }
        }
    }

    static class ServerSide implements Callable<String> {
        private final SSLServerSocket m_serverSocket;
        private final CountDownLatch m_start;
        private final CountDownLatch m_stop;

        public ServerSide(SSLServerSocket serverSocket, CountDownLatch start, CountDownLatch stop) {
            m_serverSocket = serverSocket;
            m_start = start;
            m_stop = stop;
        }

        @Override
        public String call() throws Exception {
            m_start.await();

            try (SSLSocket client = (SSLSocket) m_serverSocket.accept(); OutputStream os = client.getOutputStream(); InputStream is = client.getInputStream()) {
                System.out.println("[SERVER] Client connected. Awaiting command...");

                byte[] buf = new byte[PDU_COMMAND.length];
                int read = is.read(buf);
                if (read != buf.length) {
                    throw new RuntimeException("[SERVER] Not enough bytes read: " + read + ", expected " + buf.length + " bytes, got " + read + "!");
                }

                String command = new String(buf);

                System.out.println("[SERVER] Command received. Sending response...");

                os.write(PDU_RESPONSE);
                os.flush();

                System.out.println("[SERVER] Response written. Ending server...");

                return command;
            }
            catch (Exception e) {
                System.err.println("[SERVER] Caught exception with message: " + e.getMessage());
                throw e;
            }
            finally {
                m_stop.countDown();
            }
        }
    }

    protected static final String PIN_ALGORITHM = "SHA-256";
    protected static final int SERVER_PORT = 9000;

    static final byte[] PDU_COMMAND;
    static final byte[] PDU_RESPONSE;

    static {
        try {
            PDU_COMMAND = "ping".getBytes("ASCII");
            PDU_RESPONSE = "pong".getBytes("ASCII");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("ASCII charset encoding not supported?!");
        }
    }

    protected AbstractPinnedSSLSocketFactoryTest() {
        // Nop
    }

    protected static void performPingPongTest(X509KeyManager serverKM, Pin pinTrustedByServer, X509KeyManager clientKM, Pin pinTrustedByClient) throws Exception {
        SSLServerSocketFactory ssf = createSSLServerSocketFactory(serverKM, pinTrustedByServer);
        SSLSocketFactory csf = createSSLSocketFactory(clientKM, pinTrustedByClient);

        try (SSLServerSocket serverSocket = (SSLServerSocket) ssf.createServerSocket(SERVER_PORT);
                        SSLSocket clientSocket = (SSLSocket) csf.createSocket()) {
            serverSocket.setNeedClientAuth(pinTrustedByServer != null);
            serverSocket.setReuseAddress(true);

            CountDownLatch start = new CountDownLatch(1);
            CountDownLatch stop = new CountDownLatch(2);

            ExecutorService executor = Executors.newFixedThreadPool(2);
            Future<String> f1 = executor.submit(new ClientSide(clientSocket, start, stop));
            Future<String> f2 = executor.submit(new ServerSide(serverSocket, start, stop));

            start.countDown();

            assertTrue(stop.await(5, TimeUnit.SECONDS));

            assertEquals(Arrays.asList(new String(PDU_RESPONSE), new String(PDU_COMMAND)), awaitResults(f1, f2));
        }
    }

    @SafeVarargs
    private static <T> List<T> awaitResults(Future<T>... futures) throws Exception {
        List<T> results = new ArrayList<>();
        List<Exception> exceptions = new ArrayList<>();
        for (Future<T> future : futures) {
            try {
                results.add(future.get());
            }
            catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                exceptions.add(e);
            }
            catch (ExecutionException e) {
                exceptions.add((Exception) e.getCause());
            }
        }
        if (!exceptions.isEmpty()) {
            Exception e = exceptions.get(0);
            for (int i = 1; i < exceptions.size(); i++) {
                e.addSuppressed(exceptions.get(i));
            }
            throw e;
        }
        return results;
    }

    private static SSLServerSocketFactory createSSLServerSocketFactory(X509KeyManager serverKM, Pin pinTrustedByServer) throws GeneralSecurityException {
        SSLServerSocketFactory ssf;
        if (pinTrustedByServer != null) {
            ssf = new PinnedSSLServerSocketFactory(serverKM, pinTrustedByServer);
        }
        else {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(new KeyManager[] { serverKM }, null, null);
            ssf = sslContext.getServerSocketFactory();
        }
        return ssf;
    }

    private static SSLSocketFactory createSSLSocketFactory(X509KeyManager clientKM, Pin pinTrustedByClient) throws GeneralSecurityException {
        return new PinnedSSLSocketFactory(Arrays.asList(pinTrustedByClient), clientKM);
    }
}
