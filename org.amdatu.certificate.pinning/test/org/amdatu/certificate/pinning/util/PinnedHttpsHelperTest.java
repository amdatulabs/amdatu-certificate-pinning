/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning.util;

import static org.amdatu.certificate.pinning.PinTestUtils.generateMockCertificate;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;

import org.amdatu.certificate.pinning.CertificatePin;
import org.amdatu.certificate.pinning.Pin;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test cases for {@link PinnedHttpsHelper}.
 */
public class PinnedHttpsHelperTest {
    @Rule
    public ExpectedException m_expected = ExpectedException.none();

    @Test
    public void testOpenIncorrectlyPinnedHttpsCertFail() throws Exception {
        URL url = new URL("https://www.google.com");

        X509Certificate fakeCert = generateMockCertificate();
        Pin pin = new CertificatePin("SHA-256", fakeCert);

        HttpsURLConnection conn = PinnedHttpsHelper.openConnection(url, pin);
        assertNotNull(conn);

        // because the pin we've provided is garbage, we never should be able to connect...
        m_expected.expect(SSLHandshakeException.class);

        try {
            conn.connect(); // should succeed!
        }
        finally {
            conn.disconnect();
        }
    }

    @Test
    public void testOpenCorrectlyPinnedHttpsCertOk() throws Exception {
        URL url = new URL("https://www.google.com");
        Pin pin = createPin(url);

        HttpsURLConnection conn = PinnedHttpsHelper.openConnection(url, pin);
        assertNotNull(conn);

        try {
            conn.connect(); // should succeed!
        }
        finally {
            conn.disconnect();
        }
    }

    private Pin createPin(URL url) throws IOException, CertificateException {
        HttpsURLConnection conn = null;
        try {
            conn = (HttpsURLConnection) url.openConnection();
            conn.connect();

            Certificate[] certs = conn.getServerCertificates();
            assertTrue(certs.length >= 1);
            return new CertificatePin("SHA-256", certs[0]);
        }
        finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }
}
