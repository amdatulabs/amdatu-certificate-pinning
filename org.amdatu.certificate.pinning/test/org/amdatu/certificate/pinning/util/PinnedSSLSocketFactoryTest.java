/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.certificate.pinning.util;

import static org.amdatu.certificate.pinning.PinTestUtils.createX509KeyManager;
import static org.amdatu.certificate.pinning.PinTestUtils.generateMockPin;
import static org.amdatu.certificate.pinning.PinTestUtils.getDefaultCertificate;
import static org.amdatu.certificate.pinning.PinTestUtils.matchExceptionWithMessage;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.X509KeyManager;

import org.amdatu.certificate.pinning.CertificatePin;
import org.amdatu.certificate.pinning.Pin;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test cases for both {@link PinnedSSLServerSocketFactory} and {@link PinnedSSLSocketFactory}.
 */
public class PinnedSSLSocketFactoryTest extends AbstractPinnedSSLSocketFactoryTest {
    @Rule
    public ExpectedException m_expected = ExpectedException.none();

    private X509KeyManager m_serverKM;
    private X509KeyManager m_clientKM;
    private Pin m_serverPin;

    @Before
    public void setUp() throws Exception {
        m_serverKM = createX509KeyManager("cn=server");
        m_serverPin = new CertificatePin(PIN_ALGORITHM, getDefaultCertificate(m_serverKM));

        m_clientKM = createX509KeyManager("cn=client");
    }

    /**
     * Creates a one-way SSL connection to a trusted/pinned server socket.
     */
    @Test
    public void testCreatePinnedSocketConnectionOk() throws Exception {
        Pin trustedServerPin = null;
        Pin trustedClientPin = m_serverPin;

        // The server does not pin the client, but the client does trust the server...
        performPingPongTest(m_serverKM, trustedServerPin, m_clientKM, trustedClientPin);
    }

    /**
     * Creates a one-way SSL connection to an untrusted/unpinned server socket. This connection should not be
     * successful.
     */
    @Test
    public void testCreateUnpinnedSocketConnectionFail() throws Exception {
        Pin trustedServerPin = null;
        Pin trustedClientPin = generateMockPin();

        // because the pin the client uses does not match the certificate of the server, our SSL handshake should
        // fail...
        m_expected.expect(matchExceptionWithMessage(SSLHandshakeException.class, "Certificate (DN=CN=server issued by CN=server) not pinned!"));

        performPingPongTest(m_serverKM, trustedServerPin, m_clientKM, trustedClientPin);
    }
}
